import { Kitchen_Pantry } from './kitchen_pantry.js'
import { Plot } from './plot.js'


class City {
  constructor(data, number_of_kitchens, max_iterations, headless) {
    this.nok = number_of_kitchens
    this.data = data
    this.max_iterations = max_iterations
    this.iterations = 0
    this.headless = false || headless
    this.run_model = true
    this.kitchens = []
    this.marker

    this.start = false
    this.pantry = null

    this.side_menu = document.querySelector('.side-menu')
    this.pantry_div = document.querySelector('.pantry')
    this.map = L.map('map', {
      center: [47.5568, 7.5975],
      zoom: 14,
      scrollWheelZoom: false
    })

    L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 19,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }).addTo(this.map);

    this.map.on('click', this.add_marker.bind(this))

    this.plot = new Plot()

    if(this.headless){
      this.run_headless()
    }


  }

  add_marker(e) {
    this.nok--
    const lat = e.latlng.lat
    const lng = e.latlng.lng
    if (this.nok >= 0) {
      const marker = L.marker([lat, lng])
      marker.on('click', this.know_kitchen.bind(this))
      marker.addTo(this.map)
      const id = marker._leaflet_id
      const kitchen = new Kitchen_Pantry(this.data, id, marker)
      this.kitchens.push(kitchen)
      // start day to day advancement
    }

  }

  know_kitchen(e) {
    console.log('clicked a marker');
    const id = e.target._leaflet_id
    for (let i = 0; i < this.kitchens.length; i++) {
      const kitchen = this.kitchens[i];
      if (kitchen.id === id) {
        console.log(`found a match after ${i} attempts`);
        // set this kitchen to be shown on side bar
        this.pantry = kitchen
        // this.side_menu.style.width = '400px'

        this.plot.init_chart(this.pantry.know_memory())

        break;
      }
    }
  }

  init_kitchens() {
    for (let i = 0; i < this.nok; i++) {
      this.kitchens.push(new Kitchen_Pantry(this.data))
    }
  }

  run_headless(){
    this.init_kitchens()
    const all_kitchens_data = []
    for(let days = 0; days < this.max_iterations; days++){
      for (let i = 0; i < this.kitchens.length; i++) {
        const kitchen = this.kitchens[i];
        const id = String(i).padStart(4, '0')

        kitchen.do_breakfast_lunch_dinner()
        const obj = {
          id,
          data: kitchen.know_memory(),
          phrase: this.make_phrase(kitchen.know_memory())
        }
        all_kitchens_data.push(obj)
      }
    }
    console.log(all_kitchens_data);
  }

  make_phrase(memory){
    const incipit = 'picture of a kitchen with: '
    const partial = Object.keys(memory[0]).slice(1, 5)
    // console.log(partial);
    const words = partial.toString().replace(/\,/g, ', ')
    // console.log(words);
    return incipit + words
  }

  next_day() {
    for (let i = 0; i < this.kitchens.length; i++) {
      const kitchen = this.kitchens[i];
      kitchen.do_breakfast_lunch_dinner()
    }
    if (this.run_model) {
      // console.log('next_day');
      
      this.iterations++
      if (this.iterations === this.max_iterations) {
        this.run_model = false
        console.log('model stopped')
        const all_kitchens = []
        for (let i = 0; i < this.kitchens.length; i++) {
          const kitchen = this.kitchens[i];
          all_kitchens.push(kitchen.know_memory())
          console.log(all_kitchens);
        }
        
      }
    }
    if (this.pantry !== null) this.display_pantry(this.pantry.know())
  }

  display_pantry(pantry) {
    this.pantry_div.innerHTML = ''
    const food_items = Object.keys(pantry)
    for (let i = 0; i < food_items.length; i++) {
      const food_item = food_items[i];
      const value = pantry[food_item]
      const food_div = document.createElement('div')
      food_div.classList.add('food-name')
      food_div.textContent = food_item + ': '
      const val_div = document.createElement('div')
      val_div.classList.add('food-value')
      val_div.textContent = Math.round(value)
      const p = document.createElement('div')
      p.classList.add('pantry-item')
      p.appendChild(food_div)
      p.appendChild(val_div)
      this.pantry_div.appendChild(p)
    }
  }

}

export { City }