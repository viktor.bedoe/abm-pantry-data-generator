class Plot {

  constructor() {
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
    // this Object below contains   //
    // all the data features needed //
    // to plot the information      //
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
    this.data = null
    /// ~~~ CHARTS ~~~ ///
    this.chart
    // this.bar_chart
    /// ~~~ initialize the charts ~~~ ///
    // this.init_chart(data)
    // this.init_bar_chart()
  }
  /**
     * @returns width and height of the HTML container element
     */
  get_dims() {
    return {
      w: document.getElementById('chart').getBoundingClientRect().width,
      h: 900
    }
  }

  /**
   * initializes the chart according to the c3.js standards
   */
  init_chart(data) {

    // reset chart
    this.data = data
    document.getElementById('chart').innerHTML = ''
    this.chart = null
    this.chart = c3.generate({
      bindto: '#chart',
      size: {
        width: this.get_dims().w,
        height: 800
        // width: this.get_dims().w * 0.95,
        // height: this.get_dims().h * 0.95
      },
      padding: {
        top: 20,
        right: 75,
        bottom: 20,
        left: 75
      },
      data: {
        // x: 'datum',
        columns: [],
      },
      color: {
        pattern: ['#ff3333', '#33ff33', '#3333ff']
      },
      subchart: {
        show: true
      },
      point: {
        r: 2,
        focus: {
          expand: {
            r: 5
          }
        }
      },
      transition: {
        duration: null
      },
      onresize: () => {
        this.chart.resize({
          width: this.get_dims().w * 0.95,
          height: 800
        })
      }
    })
    this.update_chart()
  }



  init_bar_chart() {
    this.bar_chart = c3.generate({
      bindto: '#bar-chart',
      size: {
        width: 1000,
        height: 650
        // width: this.get_dims().w * 0.95,
        // height: this.get_dims().h * 0.75
      },
      padding: {
        top: 75,
        right: 75,
        bottom: 75,
        left: 75
      },
      data: {
        json: {},
        // columns: [],
        type: 'bar',
        order: null
      },
      keys: {
        x: 'name',
        value: ['value']
      },
      axis: {
        x: {
          type: 'category'
        }
      },
      color: {
        pattern: ['#00f', '#f00']
      },
      legend: {
        show: false
      },
    })
  }

  /**
   * updates all the values that are passed to the chart object
   */
  update_chart() {
    // this.parse_data(this.data)
    // console.log(this.parse_data(this.data))
    const data = JSON.parse(JSON.stringify(this.data))
    this.chart.load({
      columns: this.parse_data(data)
    })

    // this.bar_chart.load({
    //   json: this.parse_data_happiness(data['agents_happiness']),
    //   keys: {
    //     x: 'name',
    //     value: ['value']
    //   }
    // })
  }

  /**
   * convert the data so that is readable for the c3.js
   * plot generator
   * @param {Array} array
   */
  parse_data(array) {
    console.log(array);
    const result = []
    const keys = Object.keys(array[0])
    for (let i = 0; i < keys.length; i++) {
      const key = keys[i];
      const tmp = []
      for (let j = 0; j < array.length; j++) {
        const element = array[j];
        const value = element[key]
        tmp.push(value)
      }
      tmp.unshift(key)
      result.push(tmp)
    }
    // arrect.keys(arr).forEach(key => {
    //   const arr = [...arr[key]]
    //   arr.unshift(key);
    //   values.push(arr);
    // })
    return result
  }
}

export { Plot }