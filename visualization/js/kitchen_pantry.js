import { Pantry_Owner } from './pantry_owner.js'
class Kitchen_Pantry {
  constructor(data, id, marker) {
    this.data = data
    this.id = id
    this.marker = marker
    this.owner = new Pantry_Owner()
    this.vocabulary = {
      elderly: 'Elderly', adults: 'Adults', children: 'Children', adolescents: 'Adolescents'
    }
    this.habits = {}
    this.restock = 2 + Math.floor(Math.random() * 9)
    this.days_passed_from_restock = 0
    this.pantry = {}
    // console.log(this.restock)
    this.dataset = []
    this.make_food_habits()
    this.restock_pantry()
  }
  /**
   * diet from https://www.euroveg.eu/relevance/
   * 61% omnivore
   * 30% flexi
   * 5% vegetarian
   * 3% pescetarian
   * 2% vegan
   */
  make_food_habits() {
    const members = this.owner.know()
    const food_label = 'Foodex-L2'
    const consumer_perc = '%-Consumers'
    Object.keys(members).forEach(member => {
      const number_of = members[member]
      // console.log(member + ': ' + number_of);
      if (number_of > 0) {
        const data_field = this.vocabulary[member]
        const habits = this.data[data_field]
        // console.log(habits);
        Object.keys(habits).forEach(food_category => {
          const habit = habits[food_category]
          // console.log(food_category);
          // console.log(habit);
          const percentile = this.get_percentile()
          const keys = Object.keys(habit[food_label])
          // console.log(keys);
          for (const key of keys) {
            const food_item = habit[food_label][key]
            const grams_per_day = habit[percentile][key]
            const probability = habit[consumer_perc][key]
            if (Math.random() <= probability) {
              // consuming this food item
              // console.log(`${number_of} ${member} of ${percentile} perc: consume ${grams_per_day * number_of} gr of ${food_item}`);

              if (this.habits[food_item] === undefined) {
                this.habits[food_item] = grams_per_day * number_of
              } else {
                this.habits[food_item] += grams_per_day * number_of
              }
            }
            // console.log(food_item);
          }
          // console.log(percentile);
        })
      }
    })
    // console.log(this.habits);
  }

  get_percentile() {
    const perc = Math.random() * 100
    if (perc < 50) {
      // less than 50%
      if (perc <= 10) {
        if (perc <= 5) {
          // less than 5
          return 'P5'
        } else {
          // less than 10
          return 'P10'
        }
      }
      return 'Median'
    } else {
      // over 50%
      if (perc >= 99) {
        return 'P99'
      } else if (perc >= 97.5) {
        return 'P97.5'
      } else if (perc >= 95) {
        return 'P95'
      }
      return 'Median'
    }
  }

  do_breakfast_lunch_dinner() {
    // console.log(this.pantry);
    const keys = Object.keys(this.pantry)
    for (let i = 0; i < keys.length; i++) {
      const food_item = keys[i];
      this.pantry[food_item] -= this.habits[food_item]
      if (this.pantry[food_item] < 0) this.pantry[food_item] = 0
    }
    this.days_passed_from_restock++
    if (this.days_passed_from_restock === this.restock) {
      this.days_passed_from_restock = 0
      this.restock_pantry()
    }
    this.dataset.push(JSON.parse(JSON.stringify(this.pantry)))
    if(this.dataset.length > 750){
      this.dataset.splice(0, 1)
    }
    // console.log(this.pantry);
  }

  round_to_100gr(val) {
    let result = Math.round(val / 100) * 100
    if (result === 0) result = 100
    return result
  }

  restock_pantry() {
    // console.log('restock pantry');
    Object.keys(this.habits).forEach(key => {
      const amount_of_food = this.round_to_100gr(this.habits[key] * this.restock)
      if (this.pantry[key] === undefined) {
        this.pantry[key] = amount_of_food
      } else{
        if(this.restock_this(this.pantry[key], amount_of_food))this.pantry[key] += this.round_to_100gr(amount_of_food)
      }
    })
    // console.log(this.pantry);
  }

  restock_this(pantry, amount_of_food){
    // console.log(`
    // pantry: ${pantry}
    // restock: ${amount_of_food}
    // sub: ${(amount_of_food - pantry)}
    // result: ${(amount_of_food - pantry) >= 100}
    // `);
    return (amount_of_food - pantry) >= 100
  }

  know(){
    return this.pantry
  }

  know_memory(){
    return this.dataset
  }

  update_food_habits() {

  }

}

export { Kitchen_Pantry }