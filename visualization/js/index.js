
import { City } from './city.js'

class App {
  constructor(url, headless){
    this.city = null
    this.load_data(url)
    this.headless = headless
  }
  load_data(url) {
    fetch(url)
      .then(res => res.json())
      .then(json => this.build_common(json))
      .catch(err => console.log(err))
  }

  build_common(data) {
    this.city = new City(data['Austria'], 10, 365, this.headless)
  }

  new_day(){
    // if(this.city !== null && this.headless === false)this.city.next_day()
    if(this.city !== null)this.city.next_day()
  }
}



const app = new App('../reduced-consumer-data.json', false)

function progress(){
  app.new_day()
  window.requestAnimationFrame(progress)
}

window.requestAnimationFrame(progress)

// window.onclick = (e)=>{
//   app.new_day()
// }