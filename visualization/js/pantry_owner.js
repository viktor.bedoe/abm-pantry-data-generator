class Pantry_Owner {
  constructor() {
    this.pantry_owner = {
      elderly: 0, adults: 0, children: 0, adolescents: 0
    }

    this.init_pantry_owner()
  }
  /**
   * build the composition of the 
   * owners of the pantry, family, couple,
   * single parent, student, etc.
   * data from https://www.oecd.org/els/family/database.htm
   * https://en.wikipedia.org/wiki/Demographics_of_the_European_Union
   * 
   *                  living w/   cohabiting  alone
   *                   partner
   * Eurozone average	51.10	      8.05	      40.85
   * 
   *              0 kids  1 kid   2 kids  3 kids  kids<6years
   * EU average		68.63   14.87   12.34   4.16    11.87
   * EU age over 65 = 16.81% (2006)
   */
  init_pantry_owner() {
    // is a the owner single or not?
    if (this.percentage() < 59.15) {
      // single person
      // over 65?
      if (this.percentage() < 16.81) {
        // over 65
        // console.log('over 65 single');
        this.pantry_owner.elderly = 1
        // console.log(this.pantry_owner);
      } else {
        // single Adult
        // console.log('single adult');
        let kids = this.how_many_kids()
        // console.log(kids);
        kids = this.kids_composition(kids)
        // console.log(kids);
        this.pantry_owner.adults = 1
        this.pantry_owner.children = kids.children
        this.pantry_owner.adolescents = kids.adolescents
        // console.log(this.pantry_owner);
      }
    } else {
      // over 65?
      if (this.percentage() < 16.81) {
        // over 65
        // console.log('over 65 couple');
        this.pantry_owner.elderly = 2
        // console.log(this.pantry_owner);
      } else {
        // console.log('couple with kids')
        let kids = this.how_many_kids()
        // console.log(kids);
        kids = this.kids_composition(kids)
        // console.log(kids);
        this.pantry_owner.adults = 2
        this.pantry_owner.children = kids.children
        this.pantry_owner.adolescents = kids.adolescents
        // console.log(this.pantry_owner);
      }
    }
  }

  how_many_kids() {
    if (this.percentage() < 4.16) {
      // 3 kids
      return 3
    } else if (this.percentage() < 12.34) {
      //2 kids
      return 2
    } else if (this.percentage() < 14.34) {
      // 1 kid
      return 1
    } else {
      // single parent
      return 0
    }
  }
  kids_composition(kids) {
    let children = 0
    let adolescents = 0
    for (let i = 0; i < kids; i++) {
      if (this.percentage() < 11.87) {
        // kid
        children++
      } else {
        // adolescent
        adolescents++
      }
    }
    return { children, adolescents }
  }
  /**
   * next step see whether the owners 
   * change over time, a child becomes an
   * adolescent, and adult an elderly
   * and so on...
   */
  update_pantry_owner() {

  }

  know() {
    return this.pantry_owner
  }

  percentage() {
    return Math.random() * 100
  }

}

export { Pantry_Owner }